# menggunakan datadog untuk melihat log docker kontainer
pertama install agent untuk server linux sesuai prosedur ini
https://us5.datadoghq.com/account/settings/agent/latest?platform=ubuntu

kemudian apa bila service datadog sudah berjalan pada server ubuntu
tambahkan command pada /etc/datadog-agent/datadog.yaml
```
logs_enabled: true

logs_config:
  container_collect_all: true
```